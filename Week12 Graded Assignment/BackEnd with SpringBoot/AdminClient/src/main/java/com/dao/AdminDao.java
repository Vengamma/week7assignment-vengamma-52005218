package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bean.AdminDetails;

@Repository
public interface AdminDao extends JpaRepository<AdminDetails, String> {

	public AdminDetails findByUsernameAndPassword(@Param("username") String user,@Param("password") String pass);
}
