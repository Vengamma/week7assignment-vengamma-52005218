import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-admin-control',
  templateUrl: './admin-control.component.html',
  styleUrls: ['./admin-control.component.css']
})
export class AdminControlComponent implements OnInit {
  
  regadmin:string="";
  loginadmin:string="";
  constructor(public adser:AdminService) { }

  ngOnInit(): void {
  }
  adminRegister(adminRef:NgForm){
    this.adser.adminSignUp(adminRef.value).subscribe(res=>this.regadmin=res,er=>console.log(er),()=>console.log("Registred")); 
  adminRef.reset();  
  }
  
  adminlogin(adminlogin:NgForm){
    let v = this.adser.adminSignIn(adminlogin.value).subscribe(res=>this.loginadmin=res,er=>console.log(er),()=>console.log("Logged in successfully"));
    console.log(v);
    adminlogin.reset();
  }

}
