import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Admindetails} from './admindetails';
import { Observable } from 'rxjs/internal/Observable';
@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(public http:HttpClient) { }

  adminSignUp(admin:Admindetails):Observable<string>{
    return this.http.post("http://localhost:8181/AdminActivity/signUp",admin,{responseType:'text'});
  }

  adminSignIn(admin:Admindetails):Observable<string>{
    return this.http.post("http://localhost:8181/AdminActivity/signIn",admin,{responseType:'text'});
  }
  

}
