import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adminhomepage',
  templateUrl: './adminhomepage.component.html',
  styleUrls: ['./adminhomepage.component.css']
})
export class AdminhomepageComponent implements OnInit {
   
   admin:string="";
   
  constructor(public route:Router) { }

  ngOnInit(): void {
    let obj = sessionStorage.getItem("adminname");
    if(obj!=null){
      this.admin=obj;
    }
  }

  logout() {
    sessionStorage.removeItem("uname");
    this.route.navigate(["adminlogin"]);
}


}
