import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminControlComponent } from './admin-control/admin-control.component';
import { UsercontrolComponent } from './usercontrol/usercontrol.component';

const routes: Routes = [
  {path:"adminlogin", component:AdminControlComponent},
  {path:"userlogin",component:UsercontrolComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
