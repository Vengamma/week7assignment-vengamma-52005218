export class UserEntity {
    constructor(
        public username:string,
        public password:string,
        public surename:string,
        public address:string,
        public contact:number,
        public city:string,
        public state:string
    ){

    }
}
