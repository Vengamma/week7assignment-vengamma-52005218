import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { UserEntity } from './user-entity';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(public http:HttpClient) { }

  userSignUp(user:UserEntity):Observable<string>{
     return this.http.post("http://localhost:8282/UserActivity/signUp",user,{responseType:'text'});
  }

  userSignIn(user:UserEntity):Observable<string>{
   return this.http.post("http://localhost:8282/UserActivity/signIn",user,{responseType:'text'});
  }
}
