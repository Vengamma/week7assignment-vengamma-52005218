import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../user.service';

@Component({
  selector: 'app-usercontrol',
  templateUrl: './usercontrol.component.html',
  styleUrls: ['./usercontrol.component.css']
})
export class UsercontrolComponent implements OnInit {

  userReg:string="";
  userLog:string="";
  constructor(public usSer:UserService) { }

  ngOnInit(): void {
  }

  
  userRegister(userRef:NgForm){
    this.usSer.userSignUp(userRef.value).subscribe(res=>this.userReg=res,er=>console.log(er),()=>console.log("User Registred")); 
    userRef.reset();  
  }
  
  userLogin(uslogin:NgForm){
    this.usSer.userSignIn(uslogin.value).subscribe(res=>this.userLog=res,er=>console.log(er),()=>console.log("Logged in successfully"));
    uslogin.reset();
  }


}
