create database week12;
use week12;

create table adminentity(username varchar(40) primary key not null, password varchar(40) not null);
insert into adminentity values("fahad278@gmail.com","fahad723");

create table userentity(username varchar(50) primary key not null,password varchar(40) not null,surename varchar(40) not null,address varchar(200),contact long,city varchar(40),state varchar(40));
insert into userentity values("sripacha@gmail.com","Sri@199","Pacha","4-64,Ntr nagar street,Gundemadakala,vinjamur",7893446260,"nellore","AP");

create table bookentity(id int primary key not null,bookname varchar(50) not null,author varchar(50),bookgenre varchar(40),bookimage varchar(1000),price float);
insert into bookentity values(100,"Love And Life","Shakesphere","Romantic","not available",1200);
