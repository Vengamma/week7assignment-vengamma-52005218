create database bookstore;
use bookstore;
create table BookUsers(
username varchar(30) primary key  ,
password varchar(30)
);
drop table bookusers;

create table Books(
id int primary key,
name varchar(30),
genre varchar(30)
);
drop table books;

insert into books values(1001, "The sound and the fury", "Classics");
insert into books values(1002, "Catch-22",  "Horror");
insert into books values(1003, "darkness at noon",  "Romance");
insert into books values(1004,"sons and lovers",  "Autobiography");
insert into books values(1005, "under the volcano", "Autobiography");
insert into books values(1006, "1984", "Sci-Fi");
insert into books values(1007, "the way of all flesh",  "Political Satire");
insert into books values(1008, "to the lighthouse",  "Self-Help");
insert into books values(1009, "claudius", "Motivational");
insert into books values(1010, "A passage to India",  "Self-Help");


create table ReadLater(
id int primary key,
name varchar(30),
genre varchar(30)
);
drop table readlater;

create table Likelist(
id int primary key,
name varchar(30),
genre varchar(30)
);
drop table likelist;