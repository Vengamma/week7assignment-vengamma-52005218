package com.controller.user;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.user.UserDao;
import com.model.user.User;
public class LaterServlet extends HttpServlet{
private UserDao userDao;
    
    public void init() {
    	try {
			userDao = new UserDao();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
    }
    
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	doGet(request,response);
    }
	
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	    	
    	String id = request.getParameter("laterbookid");
		User user = UserLogin.currentUser;
		String name = user.getName();
		String password = user.getPassword();
		
		try {
			userDao.addLater(name, password, id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		pw.println("<html><body><h1>Book added successfully to Read later section</h1></body></html>");

		
	}

}


