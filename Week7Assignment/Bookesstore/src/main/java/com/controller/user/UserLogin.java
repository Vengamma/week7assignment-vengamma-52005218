package com.controller.user;
import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.user.UserDao;
import com.model.user.User;

@WebServlet("/UserLogin")
public class UserLogin extends HttpServlet {
private static final long serialVersionUID = 1L;
	
	public static User currentUser;
       
	private UserDao userDao;
    
    public void init() {
    	try {
			userDao = new UserDao();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
    }
	
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String name;
	String password;
	name = request.getParameter("luname");
	password = request.getParameter("lpwd");
	
	
	
	try {
		if(userDao.getUser(name,password)) {
			

			currentUser = new User();
			currentUser.setName(name);
			currentUser.setPassword(password);
			response.sendRedirect("UserData.jsp");
			
		}else {
			response.sendRedirect("LoginFail.jsp");
			
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	
	
	
	
	}
	
}
