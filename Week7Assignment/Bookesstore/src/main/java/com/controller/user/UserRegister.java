package com.controller.user;
import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.user.UserDao;
import com.model.user.User;

@WebServlet("/UserRegister")

public class UserRegister extends HttpServlet{
	private static final long serialVersionUID = 1L;
	private UserDao userDao;
       
    public void init() {
    	try {
			userDao = new UserDao();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
    }
	
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String name;
	String password;
	name = request.getParameter("runame");

	password = request.getParameter("rpwd");
	
	try {
		if(userDao.getUser(name,password)) {
			response.sendRedirect("UserExist.jsp");
		}else {		
			User user = new User();
			user.setName(name);
			user.setPassword(password);
			try {
				userDao.userRegisteration(user);
				response.sendRedirect("RegSuccess.jsp");

			} catch (SQLException e) {
				e.printStackTrace();
			}	
		
		}
	} catch (SQLException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
	}
}


