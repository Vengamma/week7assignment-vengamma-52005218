<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<jsp:include page="NameOnEveryPage.jsp" />
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<style>
            .btn {
              background-color: rgb(94, 169, 204);
              border-color: blue;
              color: white;
              padding: 12px 16px;
              font-size: 18px;
              cursor: pointer;
              border-radius: 12px;
              width: 150px;
              margin: 15px;

            }
            
            /* Darker background on mouse-over */
            .btn:hover {
              background-color: rgb(5, 44, 160);
            }
            </style>


</body>
</html>
<%

try {
Class.forName("com.mysql.cj.jdbc.Driver");
} catch (ClassNotFoundException e) {
e.printStackTrace();
}

Connection connection = null;
Statement statement = null;
ResultSet resultSet = null;
%>
<div>
<a href="ReadLater.jsp"><button class="btn"><b>Read Later</b></button></a>
<a href="Liked.jsp" ><button class="btn"><b>Liked</b></button></a>
<a href="logout.jsp" ><button class="btn"><b>Logout</b></button></a>

</div>
<h2 align="center"><font><strong>Books List</strong></font></h2>
<table align="center" cellpadding="5" cellspacing="5" border="1">
<tr>

</tr>
<tr bgcolor="#A52A2A">
<td><b>id</b></td>
<td><b>Books name</b></td>
<td><b>Read later</b></td>
<td><b>Like</b></td>

</tr>
<%
try{ 
connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore","root","root");
statement=connection.createStatement();

resultSet = statement.executeQuery("SELECT * FROM books");
while(resultSet.next()){
%>
<tr bgcolor="#DEB887">

<td><%=resultSet.getString("bookId") %></td>
<td><%=resultSet.getString("bookName") %></td>

<td><form action="later">
<input type = "hidden" name = "laterbookid" value = <%= resultSet.getString("bookId") %> />
<input type = "submit" value="Read Later">
</form></td>

<td><form action="liked">
<input type = "hidden" name = "likedbookid" value = <%= resultSet.getString("bookId") %> />
<input type = "submit" value="Like">
</form></td>
</tr>

<% 
}

} catch (Exception e) {
e.printStackTrace();
}
%>
</table>
