<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Logout File</title>
</head>
<body>
<h1>Logout Successfully</h1>
<%
session.invalidate();
response.sendRedirect("index.html");
%>
</body>
</html>