-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: localhost    Database: books
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `book` (
  `bookId` int NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `author` varchar(181) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `pagecount` int NOT NULL,
  `poster` text,
  PRIMARY KEY (`bookId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,'A Daughter of the Snows','Jack London','Guide',199,'A Daughter of the Snows.jpg'),(2,'The Near East: 10,000 Years of History','Isaac Asimov','Journals',298,'TheNearEast10,000YearsofHistory.jpg'),(3,'The Cocoon: A Rest-Cure Comedy','Ruth Stuart','Diaries',90,'TheCocoonARestCureComedy.jpg'),(4,'The Freakshow Murders','Frederic Brown','Drama',321,'The Freakshow Murders.jpg'),(5,'Pharaohs, Fellahs and Explorers','Amelia B. Edwards','Guide',367,'Pharaohs, Fellahs and Explorers.jpg'),(6,'Hard Times','Charles Dickens','Science fiction',293,'Hard Times.jpg'),(7,'A Modern Instance','William Dean Howells','Diaries',222,'A Modern Instance.jpg'),(8,'The Real Mother Goose','Mother Goose','Science fiction',88,'The Real Mother Goose.jpg'),(9,'A Thousand Miles Up the Nile','Amelia B. Edwards','Art',110,'A Thousand Miles Up the Nile.jpg'),(10,'Children of Blood and Bone','Titus Lucretius','Journals',137,'Children of Blood and Bone.jpg'),(11,'A pushcart at the curb','John DosPassos','Science fiction',95,'A pushcart at the curb.jpg'),(12,'The Desert and the Sown','Mary Hallock Foote','Diaries',126,'The Desert and the Sown.jpg'),(13,'Three Soldiers','John DosPassos','History',223,'Three Soldiers.jpg'),(14,'The End of Eternity','Isaac Asimov','Science fiction',168,'The End of Eternity.jpg'),(15,'Annie Kilburn','William Dean Howells','Diaries',291,'Annie Kilburn.jpg'),(16,'A Touch of Sun and Other Stories','Mary Hallock Foote','Guide',141,'A Touch of Sun and Other Stories.jpg'),(17,'Show Boat','Edna Ferber','Art',151,'Show Boat.jpg'),(18,'The Call of the Wild','Jack London','Art',362,'The Call of the Wild.jpg'),(19,'My Mark Twain','William Dean Howells','Guide',339,'My Mark Twain.jpg'),(20,'Broken Ties','Rabindranath Tagore','Romance',134,'Broken Ties.jpg'),(21,'Short Stories From American History','Albert Blaisdell','Health',305,'Short Stories From American History.jpg'),(22,'Mrs Rosie and the Priest','Givoanni Boccaccio','Biographies',104,'Mrs Rosie and the Priest.jpg'),(23,'So Big','Edna Ferber','Drama',209,'So Big.jpg'),(24,'Monsieur Maurice ','Amelia B. Edwards','Health',92,'Monsieur Maurice.jpg'),(25,'The Master of Ballantrae','Robert Louis Stevenson','Journals',236,'The Master of Ballantrae.jpg'),(26,'The Unlived Life of Little Mary Ellen','Ruth Stuart','Mystery',99,'The Unlived Life of Little Mary Ellen.jpg'),(27,'Mouse - The Last Train','Frederic Brown','Biographies',184,'Mouse - The Last Train.jpg'),(28,'Edith Bonham','Mary Hallock Foote','Horror',122,'Edith Bonham.jpg'),(29,'Maybe Mother Goose','Mother Goose','Biographies',161,'Maybe Mother Goose.jpg'),(30,'The Noble Gases','Isaac Asimov','Romance',321,'The Noble Gases.jpg'),(31,'Rainy Week','Eleanor Hallowell Abbott','Horror',98,'Rainy Week.jpg'),(32,'A Hazard of New Fortunes','William Dean Howells','Action and Adventure',144,'A Hazard of New Fortunes.jpg'),(33,'A Plot for Murder','Frederic Brown','Journals',108,'A Plot for Murder.jpg'),(34,'Nature','Ralph Waldo Emerson','Health',357,'Nature.jpg'),(35,'Hickory Dickory Dock','Mother Goose','Mystery',326,'Hickory Dickory Dock.jpg'),(36,'Big Fat Hen','Mother Goose','Diaries',232,'Big Fat Hen.jpg'),(37,'The Lone Star Ranger','Zane Grey','Guide',328,'The Lone Star Ranger.jpg'),(38,'The Great Fire of London','Samuel Pepys','Guide',252,'The Great Fire of London.jpg'),(39,'White Nights','Fyodor Dostoevsky','Biographies',265,'White Nights.jpg'),(40,'In a Glass Darkly','Joseph Sheridan LeFanu','Guide',173,'In a Glass Darkly.jpg'),(41,'Fanny herself','Edna Ferber','Diaries',248,'Fanny herself.jpg'),(42,'Loaded','Frederic Brown','Action and Adventure',273,'Loaded.jpg'),(43,'A Foregone Conclusion','William Dean Howells','Fantasy',121,'A Foregone Conclusion.jpg'),(44,'The Hill of Dreams','Arthur Machen','Mystery',175,'The Hill of Dreams.jpg'),(45,'Adventure','Jack London','Health',217,'Adventure.jpg'),(46,'A Tagore Reader','Rabindranath Tagore','Action and Adventure',285,'A Tagore Reader.jpg'),(47,'A Tale of Two Cities','Charles Dickens','Journals',331,'A Tale of Two Cities.jpg'),(48,'Dombey and Son','Charles Dickens','Biographies',344,'Dombey and Son.jpg'),(49,'Famous Women','Givoanni Boccaccio','Science fiction',273,'Famous Women.jpg'),(50,'Rejection, The Ruling Spirit','Ellis Butler','Satire',358,'Rejection, The Ruling Spirit.jpg'),(51,'Little Dorrit','Charles Dickens','Journals',314,'Little Dorrit.jpg'),(52,'The Four-Fifteen Express','Amelia B. Edwards','Satire',201,'The Four-Fifteen Express.jpg'),(53,'Fairy Prince and Other Stories','Eleanor Hallowell Abbott','Fantasy',306,'Fairy Prince and Other Stories.jpg'),(54,'Ten Tales from the Decameron','Givoanni Boccaccio','Romance',183,'Ten Tales from the Decameron.jpg'),(55,'The Double','Fyodor Dostoevsky','Science',372,'The double.jpg'),(56,'A Flight Of Swans','Rabindranath Tagore','Health',115,'A Flight Of Swans.jpg'),(57,'The Phantom Coach','Amelia B. Edwards','Science',243,'The Phantom Coach.jpg'),(58,'The Game','Jack London','Diaries',209,'The Game.jpg'),(59,'Rico and Wiseli','Johanna Spyri','Health',309,'Rico and Wiseli.jpg'),(60,'The Shorter Pepys','Samuel Pepys','Romance',276,'The Shorter Pepys.jpg'),(61,'My Life Had Stood a Loaded Gun ','Emily Dickinson','Diaries',278,'My Life Had Stood a Loaded Gun.jpg'),(62,'A Tagore Testament','Rabindranath Tagore','Fantasy',268,'A Tagore Testament.jpg'),(63,'The Cruise of the Dazzler','Jack London','Diaries',140,'The Cruise of the Dazzler.png'),(64,'To the Lighthouse','Virginia Woolf','Satire',90,'To the Lighthouse.jpg'),(65,'The Terror','Arthur Machen','Art',293,'The Terror.jpg'),(66,'The Sick-a-Bed Lady','Eleanor Hallowell Abbott','Guide',369,'The Sick-a-Bed Lady.jpg'),(67,'Christmas Every Day','William Dean Howells','Journals',98,'Christmas Every Day.jpg'),(68,'The home-maker','Dorothy Canfield','Biographies',316,'The home-maker.jpg'),(69,'The Ground-Swell','Mary Hallock Foote','Diaries',193,'The Ground-Swell.jpg'),(70,'A Sleep and a Forgetting','William Dean Howells','Satire',360,'A Sleep and a Forgetting.jpg'),(71,'Nicholas Nickleby','Charles Dickens','Science fiction',219,'Nicholas Nickleby.jpg'),(72,'The Whole Family: A Novel by Twelve Authors','William Dean Howells','Mystery',149,'The Whole Family.jpg'),(73,'Middlemarch','George Eliot','Comics',288,'Middlemarch.jpg'),(74,'Life of Dante','Givoanni Boccaccio','Biographies',156,'Life of Dante.jpg'),(75,'Short Stories From English History','Albert Blaisdell','Satire',189,'Short Stories From English History.jpg'),(76,'Little Eve Edgarton','Eleanor Hallowell Abbott','Romance',306,'Little Eve Edgarton.jpg'),(77,'The Kingdom of the Sun','Isaac Asimov','Guide',189,'The Kingdom of the Sun.jpg'),(78,'Indian Summer','William Dean Howells','Biographies',107,'Indian Summer.jpg'),(79,'Mrs Dalloway','Virginia Woolf','Romance',321,'Mrs Dalloway.jpg'),(80,'Demons','Fyodor Dostoevsky','Fantasy',232,'Demons.jpg'),(81,'The Last Trail','Zane Grey','Biographies',116,'The Last Trail.jpg'),(82,'The Fredric Brown Megapack','Frederic Brown','History',324,'The Fredric Brown Megapack.jpg'),(83,'The Decameron: Selected Tales','Givoanni Boccaccio','Science',337,'The DecameronSelected Tales.jpg'),(84,'Th bent twig','Dorothy Canfield','Drama',367,'Th bent twig.jpg'),(85,'Things near and far','Arthur Machen','Mystery',199,'Things near and far.jpg'),(86,'The Story of Salome','Amelia B. Edwards','Comics',316,'The Story of Salome.jpg'),(87,'The Destruction of Our Children','Ellis Butler','Guide',104,'The Destruction of Our Children.jpg'),(88,'Collected Stories','Rabindranath Tagore','Action and Adventure',295,'Collected Stories.jpg'),(89,'The House of the Dead','Fyodor Dostoevsky','Satire',209,'The House of the Dead.jpg'),(90,'Sonny: A Christmas Guest','Ruth Stuart','Mystery',186,'Sonny A Christmas Guest.jpg'),(91,'Amores','Publius Ovid','Diaries',297,'Amores.jpg'),(92,'Chaturanga','Rabindranath Tagore','Diaries',218,'Chaturanga.jpg'),(93,'Little Eve Edgarton','Eleanor Hallowell Abbott','Biographies',247,'Little Eve Edgarton.jpg'),(94,'Chitra','Rabindranath Tagore','Guide',122,'Chitra.jpg'),(95,'The Iron Heel','Jack London','Fantasy',81,'The Iron Heel.jpg'),(96,'Self Reliance','Ralph Waldo Emerson','Art',137,'Self Reliance.jpg'),(97,'The Great God Pan And The Hill Of Dreams','Arthur Machen','Romance',158,'The Great God Pan And The Hill Of Dreams.jpg'),(98,'The White People and Other Weird Stories','Arthur Machen','Fantasy',100,'The White People and Other Weird Stories.jpg'),(99,'The Diary of Samuel Pepys: A Selection','Samuel Pepys','Drama',271,'The Diary of Samuel Pepys.jpg'),(100,'The Rise of Silas Lapham','William Dean Howells','Satire',314,'The Rise of Silas Lapham.jpg'),(101,'The Mystery of Edwin Drood','Charles Dickens','Diaries',340,'The Mystery of Edwin Drood.jpg'),(102,'In Exile and Other Stories','Mary Hallock Foote','Comics',140,'In Exile and Other Stories.jpg'),(103,'Bleak House','Charles Dickens','Art',130,'Bleak House.jpg'),(104,'U.S.A Trilogy','John DosPassos','Science fiction',118,'U.S.A Trilogy.jpg'),(105,'A Comedy of Masks','Ernest Dowson','Journals',237,'A Comedy of Masks.jpg'),(106,'Barnaby Rudge','Charles Dickens','Diaries',380,'Barnaby Rudge.jpg'),(107,'Heroic Deeds of American Sailors','Albert Blaisdell','Science fiction',243,'Heroic Deeds of American Sailors.jpg'),(108,'A Bouquet','Ernest Dowson','Art',309,'A Bouquet.jpg'),(109,'Editha','William Dean Howells','Art',155,'Editha.jpg'),(110,'Manhattan Transfer','John DosPassos','Drama',124,'Manhattan Transfer.jpg'),(111,'Fasti','Publius Ovid','Horror',325,'Fasti.jpg'),(112,'Hearts of Three','Jack London','Satire',272,'Hearts of Three.jpg'),(113,'Speak, Memory','Vladimir Nabokov','Fantasy',154,'Speak, Memory.jpg'),(114,'Kidnapped','Robert Louis Stevenson','Drama',334,'Kidnapped.jpg'),(115,'The Brothers Karamazov','Fyodor Dostoevsky','Mystery',161,'The Brothers Karamazov.jpg'),(116,'Envelope Poems','Emily Dickinson','Fantasy',342,'Envelope Poems.jpg'),(117,'Selected Poems','Emily Dickinson','Guide',203,'Selected Poems.jpg'),(118,'Gobolinks or Shadow Pictures for Young and Old','Ruth Stuart','Romance',332,'Gobolinks or Shadow Pictures for Young and Old.jpg'),(119,'The Decameron','Givoanni Boccaccio','Diaries',318,'The Decameron.jpg'),(120,'The Cock and Anchor','Joseph Sheridan LeFanu','Science fiction',133,'The Cock and Anchor.jpg'),(121,'Adrian Rome','Ernest Dowson','Journals',259,'Adrian Rome.jpg'),(122,'Words in Genesis','Isaac Asimov','Science',217,'Words in Genesis.jpg'),(123,'Stories of Ohio','William Dean Howells','Romance',191,'Stories of Ohio.jpg'),(124,'Treasure Island','Robert Louis Stevenson','Action and Adventure',308,'Treasure Island.jpg'),(125,'Uncle Silas','Joseph Sheridan LeFanu','Science fiction',118,'Uncle Silas.jpg'),(126,'The Idiot','Fyodor Dostoevsky','Horror',248,'The Idiot.jpg'),(127,'Inside The Atom','Isaac Asimov','Action and Adventure',176,'Inside The Atom.jpg'),(128,'The Story of Babette: A Little Creole Girl','Ruth Stuart','Romance',313,'The Story of BabetteA Little Creole Girl.jpg'),(129,'The Eternal Husband','Fyodor Dostoevsky','Fantasy',225,'The Eternal Husband.jpg'),(130,'Heidi','Johanna Spyri','Science',146,'Heidi.jpg'),(131,'The Concise Pepys','Samuel Pepys','Journals',303,'The Concise Pepys.jpg'),(132,'Tales From Shakespeare','Albert Blaisdell','Diaries',121,'Tales From Shakespeare.jpg'),(133,'Molly Make-Believe','Eleanor Hallowell Abbott','Diaries',286,'Molly Make-Believe.jpg'),(134,'Peace on Earth, Good Will to Dogs','Eleanor Hallowell Abbott','Guide',321,'Peace on Earth, Good Will to Dogs.jpg'),(135,'Miss Darkness','Frederic Brown','Health',360,'Miss Darkness.jpg'),(136,'Poor Folk','Fyodor Dostoevsky','Horror',202,'Poor Folk.jpg'),(137,'Their Wedding Journey','William Dean Howells','Art',132,'Their Wedding Journey.jpg'),(138,'The Elegy of Lady Fiammetta','Givoanni Boccaccio','Action and Adventure',157,'The Elegy of Lady Fiammetta.jpg'),(139,'Art Of Tagore','Rabindranath Tagore','History',119,'Art Of Tagore.jpg'),(140,'Chemistry and Human Health','Isaac Asimov','Journals',244,'Chemistry and Human Health.jpg'),(141,'Realm of Numbers','Isaac Asimov','Comics',279,'Realm of Numbers.jpg'),(142,'The Kempton-Wace Letters','Jack London','Drama',240,'The Kempton-Wace Letters.jpg'),(143,'Crime and Punishment','Fyodor Dostoevsky','Science',144,'Crime and Punishment.jpg'),(144,'Fairy Prince and Other Wonderful Stories','Eleanor Hallowell Abbott','Science fiction',219,'Fairy Prince and Other Wonderful Stories.jpg'),(145,'Adam Bede','George Eliot','Action and Adventure',303,'Adam Bede.jpg'),(146,'Madam Crowl\'s ghost','Joseph Sheridan LeFanu','Journals',344,'Madam Crowl\'s ghost.jpg'),(147,'Is Anyone There?','Isaac Asimov','Biographies',178,'Is Anyone There'),(148,'An Imperative Duty','William Dean Howells','Mystery',100,'An Imperative Duty.jpg'),(149,'Adding a Dimension','Isaac Asimov','Comics',334,'Adding a Dimension.jpg'),(150,'The Caves of Steel','Isaac Asimov','Art',98,'The Caves of Steel.jpg'),(151,'Carmilla','Joseph Sheridan LeFanu','Art',91,'Carmilla.jpg'),(152,'1919','John DosPassos','Science',105,'1919.jpg'),(153,'Pale Fire','Vladimir Nabokov','Journals',250,'Pale Fire.jpg'),(154,'Martin Chuzzlewit','Charles Dickens','Drama',315,'Martin Chuzzlewit.jpg'),(155,'Understood Betsy','Dorothy Canfield','Horror',130,'Understood Betsy.jpg'),(156,'The White Linen Nurse','Eleanor Hallowell Abbott','Journals',369,'The White Linen Nurse.jpg'),(157,'A Writer\'s Diary','Fyodor Dostoevsky','Science fiction',164,'A Writer\'s Diary.jpg'),(158,'Pnin','Vladimir Nabokov','Biographies',233,'Pnin.jpg'),(159,'Honeymoon in Hell','Frederic Brown','History',202,'Honeymoon in Hell.jpg'),(160,'The Black Arrow','Robert Louis Stevenson','Diaries',366,'The Black Arrow.jpg'),(161,'David Copperfield','Charles Dickens','History',240,'David Copperfield.jpg'),(162,'Famous Ghost Stories','Amelia B. Edwards','Drama',152,'Famous Ghost Stories.jpg'),(163,'Book Of Indian Braves','Kate Dickinson Sweetser','Health',117,'Book Of Indian Braves.jpg'),(164,'Binodini','Rabindranath Tagore','Health',306,'Binodini.jpg'),(165,'Holly And Pizen, And Other Stories','Ruth Stuart','Science',220,'Holly And Pizen, And Other Stories.jpg'),(166,'Lolita','Vladimir Nabokov','Satire',153,'Lolita.jpg'),(167,'The Best Short Stories','Fyodor Dostoevsky','Satire',115,'The Best Short Stories.jpg'),(168,'Cimarron','Edna Ferber','Science',101,'Cimarron.jpg');
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likedbooks`
--

DROP TABLE IF EXISTS `likedbooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `likedbooks` (
  `likedbook_id` int NOT NULL AUTO_INCREMENT,
  `bookId` int NOT NULL,
  `userId` int NOT NULL,
  PRIMARY KEY (`likedbook_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likedbooks`
--

LOCK TABLES `likedbooks` WRITE;
/*!40000 ALTER TABLE `likedbooks` DISABLE KEYS */;
INSERT INTO `likedbooks` VALUES (26,0,1),(29,4,0),(38,1,2),(39,5,2),(40,3,1);
/*!40000 ALTER TABLE `likedbooks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `readlaterbooks`
--

DROP TABLE IF EXISTS `readlaterbooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `readlaterbooks` (
  `readlaterbookId` int NOT NULL AUTO_INCREMENT,
  `bookId` int NOT NULL,
  `userId` int NOT NULL,
  PRIMARY KEY (`readlaterbookId`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `readlaterbooks`
--

LOCK TABLES `readlaterbooks` WRITE;
/*!40000 ALTER TABLE `readlaterbooks` DISABLE KEYS */;
INSERT INTO `readlaterbooks` VALUES (26,3,2),(27,6,2),(28,4,1);
/*!40000 ALTER TABLE `readlaterbooks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `userId` int NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(32) NOT NULL,
  `phone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Karthik Veeresh Tippeshetti','tkarthik642@gmail.com','123456789','+919538633097'),(2,'karthik','karthik@test.com','1234','9988774455'),(3,'karu','karu@gmail.com','9632147850','9632147850'),(4,'Ajay','Ajay@gmail.com','9874123650','9966332211'),(5,'Karthik','qwerty@gamil.com','123456789','321456970');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-30 11:31:20
