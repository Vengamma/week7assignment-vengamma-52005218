package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.Book;
import com.service.BookService;

@RestController
@RequestMapping("/book")
public class BookController {
	@Autowired
	BookService bookService;
	//http://localhost:9090/book/getAllBook
	@GetMapping(value = "getAllBook",
	produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Book> getAllBookInfo() {
		return bookService.getAllBooks();
	}
	//http://localhost:9090/book/storeBook
	@PostMapping(value = "storeBook",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeBookInfo(@RequestBody Book b) {
		
				return bookService.storeBookInfo(b);
	}
	//http://localhost:9090/book/deleteBook/0
	@DeleteMapping(value = "deleteBook/{bookid}")
	public String storeBookInfo(@PathVariable("bookid") int bookid) {
					return bookService.deleteBookInfo(bookid);
	}
	//http://localhost:9090/book/updateBook
	@PatchMapping(value = "updateBook")
	public String updateBookInfo(@RequestBody Book b) {
					return bookService.updateBookInfo(b);
	}
	//http://localhost:9090/book/findBookByBookPrice/150
	@GetMapping(value = "findBookByBookPrice/{bookprice}")
	public List<Book> findBookByBookPrice(@PathVariable("bookprice") float bookprice) {
		return bookService.findBookUsingBookPrice( bookprice);
	}
}
 

