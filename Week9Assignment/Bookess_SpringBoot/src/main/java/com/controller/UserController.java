package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.User;
import com.service.UserService;
@RestController
@RequestMapping("/user")

public class UserController {
	@Autowired
	UserService userService;
	//http://localhost:9090/user/getAllUser
	@GetMapping(value = "getAllUser",
	produces = MediaType.APPLICATION_JSON_VALUE)
	public List<User> getAllUserInfo() {
		return userService.getAllUsers();
	}
	//http://localhost:9090/user/storeUser
	@PostMapping(value = "storeUser",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeUserInfo(@RequestBody User u) {
		
				return userService.storeUserInfo(u);
	}
	//http://localhost:9090/user/deleteUser/
	@DeleteMapping(value = "deleteUser/{userid}")
	public String storeUserInfo(@PathVariable("userid") int userid) {
					return userService.deleteUserInfo(userid);
	}
	//http://localhost:9090/user/updateUser
	@PatchMapping(value = "updateUser")
	public String updateUserInfo(@RequestBody User u) {
					return userService.updateUserInfo(u);
	}
	
	
	

}
