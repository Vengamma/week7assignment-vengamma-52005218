package com.dao;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Repository;

import com.bean.Admin;
@Repository
public interface AdminDao extends JpaRepositoryImplementation<Admin, String>{
	

}
