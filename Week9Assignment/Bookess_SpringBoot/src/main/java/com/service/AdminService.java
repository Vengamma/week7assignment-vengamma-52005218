package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Admin;

import com.dao.AdminDao;

@Service
public class AdminService {
	@Autowired
	AdminDao adminDao;
	public String storeAdminInfo(Admin admin) {
		
		if(adminDao.existsById(admin.getMailid())) {
					return "Admin mail id must be unique";
		}else {
					adminDao.save(admin);
					return "admin stored successfully";
		        }
          }

       
    public String updateAdminInfo(Admin admin) {
	if(!adminDao.existsById(admin.getMailid())) {
		return "admin details not present";
		}else  {
		Admin x	= adminDao.getById(admin.getMailid());
		x.setPassword(admin.getPassword());
		x.setMailid(admin.getMailid());
		adminDao.saveAndFlush(x);
		return "Admin password updated successfully";
		}	
}
    public String storeAdminLoginInfo(Admin admin) {
		
		if(adminDao.existsById(admin.getMailid())) {
					return "login mail id must be unique";
		}else {
					adminDao.save(admin);
					return "login  successfully";
		        }
          }
    public String AdminlogOutInfo(String mailid ) {
		if(adminDao.existsById(mailid)) {
			return "Successfully Log Out";
		}
		else {
			return "Log Out failed";
		}

    
}
}
