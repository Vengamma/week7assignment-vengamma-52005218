package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.bean.User;

import com.dao.UserDao;
@Service
public class UserService {
	@Autowired
	UserDao userDao;
	
	public List<User> getAllUsers() {
		return userDao.findAll();
	}
	
	public String storeUserInfo(User u) {
				
						if(userDao.existsById(u.getUserid())) {
									return "User id must be unique";
						}else {
									userDao.save(u);
									return "User stored successfully";
						}
	}
	
	public String deleteUserInfo(int userid) {
		if(!userDao.existsById(userid)) {
			return "User details not present";
			}else {
			userDao.deleteById(userid);
			return "User deleted successfully";
			}	
	}
	
	public String updateUserInfo(User u) {
		if(!userDao.existsById(u.getUserid())) {
			return "user details not present";
			}else {
			User q	= userDao.getById(u.getUserid());	
			q.setUsername(u.getUsername());
			q.setUserid(u.getUserid());
			q.setUserpassword(u.getUserpassword());
			q.setUsermail(u.getUsermail());
			userDao.saveAndFlush(q);				
			return "user updated successfully";
			}	
	}
	
	
}


